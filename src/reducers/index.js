import { combineReducers } from 'redux';

import topStories from './topStories';

export default combineReducers({
  topStories,
});
