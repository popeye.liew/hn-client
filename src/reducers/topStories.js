import { actionTypes } from 'actions';
import { createReducer } from 'shared/helpers';

const initialState = {
  topStoryIds: [],
  topStories: [],
  itemsPerPageLimit: 30,
  totalPageItems: 0,
  currentPage: 0,
};

export default createReducer(initialState, {
  [actionTypes.GET_TOP_STORIES]: state => {
    return { ...state };
  },
  [actionTypes.GET_TOP_STORY_IDS_REQUEST]: state => {
    return { ...state };
  },
  [actionTypes.GET_TOP_STORY_IDS_SUCCESS]: (state, payload) => {
    return {
      ...state,
      topStoryIds: payload,
      totalPageItems: payload.length,
    };
  },
  [actionTypes.GET_TOP_STORY_IDS_FAILURE]: state => {
    return {
      ...state,
      topStoryIds: [],
    };
  },
  [actionTypes.GET_STORY_DETAILS_REQUEST]: (state, payload) => {
    return { ...state };
  },
  [actionTypes.GET_STORY_DETAILS_SUCCESS]: (state, payload) => {
    return { ...state };
  },
  [actionTypes.GET_STORY_DETAILS_FAILURE]: state => {
    return {
      ...state,
      topStories: [],
    };
  },
  [actionTypes.SET_TOP_STORIES]: (state, payload) => {
    return {
      ...state,
      topStories: payload,
    };
  },
  [actionTypes.CHANGE_PAGE]: (state, payload) => {
    return {
      ...state,
      topStories: [],
      currentPage: payload,
    };
  },
  [actionTypes.TEST]: (state, payload) => {
    return { ...state, payload };
  },
});