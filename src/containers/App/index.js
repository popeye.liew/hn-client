import React, { Component } from 'react';
import { Route, Redirect, Switch, withRouter } from 'react-router-dom';
import { TopStories, Story } from 'containers';
import { MainMenu } from './components';
import injectSheet from 'react-jss';

const styles = {
  mainContent: {
    padding: '4.5rem 1rem 1rem',
  },
};

class App extends Component {
  componentDidMount () {
    console.log('%c Some fancy stuff going here! ', 'padding: 4px 0; font-size: 16px; background: linear-gradient(135deg, #ff3200 0%,#f9b300 100%); color: white');
  }

  render() {
    const { classes } = this.props;

    return (
      <React.Fragment>
        <MainMenu />
        
        <div className={classes.mainContent}>
          <Switch>
            <Route path="/" exact component={TopStories} />
            <Route path="/stories/:storyId" component={Story} />
            <Redirect to="/" />
          </Switch>
        </div>
      </React.Fragment>
    );
  }
}

export default withRouter(injectSheet(styles)(App));
