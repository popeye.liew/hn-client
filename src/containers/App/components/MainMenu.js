import React from 'react';
import { NavLink, Link } from 'react-router-dom';
import injectSheet from 'react-jss';
import packageJson from '~/../../package.json';

const styles = {
  root: {
    height: '3.5rem',
    padding: '0 1rem',
    display: 'flex',
    alignItems: 'center',
    backgroundColor: 'white',
    boxShadow: '0 0 15px 0 rgba(0, 0, 0, 0.1)',
    position: 'fixed',
    left: 0,
    right: 0,
  },
  brandName: {
    fontSize: '1.5rem',
    fontWeight: 'bold',
    textTransform: 'uppercase',
    flex: '0 0 auto',
  },
  menuItems: {
    paddingLeft: '1rem',
    flex: '1 1 100%',
  },
  menuItem: {
    margin: '0 0.2rem',
    padding: '0.4rem 1rem',
    fontSize: '0.8rem',
    color: 'black',
    textDecoration: 'none',
    transition: '0.15s',
    borderRadius: '1rem',
    '&.active, &:not([disabled]):hover': {
      color: 'white',
      backgroundColor: '#ff3200',
    },
    '&[disabled]': {
      color: '#999',
      cursor: 'default',
    },
  },
  version: {
    fontSize: '0.8rem',
    color: '#999',
    flex: '0 0 auto',
    display: 'flex',
    alignItems: 'center',
  },
  gitlab: {
    width: '1rem',
    padding: '0.5rem',
  },
};

const MainMenu = ({ classes }) =>(
  <div className={classes.root}>
    <span className={classes.brandName}>HN Client</span>

    <div className={classes.menuItems}>
      <NavLink className={classes.menuItem} activeClassName="active" to="/" exact>Top Stories</NavLink>
      <NavLink className={classes.menuItem} activeClassName="active" to="/coming-soon" disabled onClick={e => e.preventDefault()}>Coming Soon</NavLink>
    </div>

    <div className={classes.version}>
      <span>V{packageJson.version}</span>
      <Link className={classes.gitlab} to="https://gitlab.com/popeye.liew/hn-client" target="_blank">
        <svg viewBox="0 0 128 128">
          <path fill="#FC6D26" d="M126.615 72.31l-7.034-21.647L105.64 7.76c-.716-2.206-3.84-2.206-4.556 0l-13.94 42.903H40.856L26.916 7.76c-.717-2.206-3.84-2.206-4.557 0L8.42 50.664 1.385 72.31a4.792 4.792 0 0 0 1.74 5.358L64 121.894l60.874-44.227a4.793 4.793 0 0 0 1.74-5.357"></path><path fill="#E24329" d="M64 121.894l23.144-71.23H40.856L64 121.893z"></path><path fill="#FC6D26" d="M64 121.894l-23.144-71.23H8.42L64 121.893z"></path><path fill="#FCA326" d="M8.42 50.663L1.384 72.31a4.79 4.79 0 0 0 1.74 5.357L64 121.894 8.42 50.664z"></path><path fill="#E24329" d="M8.42 50.663h32.436L26.916 7.76c-.717-2.206-3.84-2.206-4.557 0L8.42 50.664z"></path><path fill="#FC6D26" d="M64 121.894l23.144-71.23h32.437L64 121.893z"></path><path fill="#FCA326" d="M119.58 50.663l7.035 21.647a4.79 4.79 0 0 1-1.74 5.357L64 121.894l55.58-71.23z"></path><path fill="#E24329" d="M119.58 50.663H87.145l13.94-42.902c.717-2.206 3.84-2.206 4.557 0l13.94 42.903z"></path>
        </svg>
      </Link>
    </div>
  </div>
);

export default injectSheet(styles)(MainMenu);
