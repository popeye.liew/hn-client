import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Pagination, StoryItem } from 'shared/components';
import { getTopStories, changePage } from 'actions';

import * as _ from 'lodash';

class TopStories extends Component {
  componentDidMount() {
    document.title = `Top Stories | ${process.env.REACT_APP_NAME}`;

    this.props.getTopStories();
  }

  render() {
    const { topStories, pagination, changePage } = this.props;

    return (
      <React.Fragment>
        <Pagination pagination={pagination} onClick={changePage} />

        {topStories.map(story => (
          <StoryItem key={story.id} story={story} />
        ))}
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    topStories: state.topStories.topStories,
    pagination: {
      itemsPerPageLimit :state.topStories.itemsPerPageLimit,
      totalPageItems :state.topStories.totalPageItems,
      currentPage :state.topStories.currentPage,
    },
  }
};

const mapDispatchToProps = dispatch => {
  return {
    getTopStories: () => dispatch(getTopStories()),
    changePage: pageIdx => dispatch(changePage(pageIdx)),
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(TopStories);
