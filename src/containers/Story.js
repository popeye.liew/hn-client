import React, { Component } from 'react';
import { StoryItem, CommentItem } from 'shared/components';
import injectSheet from 'react-jss';

import * as _ from 'lodash';

const styles = {
  story: {
    marginBottm: '2rem',
    backgroundColor: 'white !important',
  },
}

class Story extends Component {
  state = {
    story: {},
    comments: [],
  };

  async componentDidMount() {
    document.title = `Story | ${process.env.REACT_APP_NAME}`;

    // Get Story details
    const { match: { params } } = this.props;
    fetch(`${process.env.REACT_APP_HN_API}item/${params.storyId}.json`)
      .then(res => res.json())
      .then(async res => {
        this.setState({ story: res });
        
        // TODO: Sad. Get all story's comments
        // const comments = await this.getComments(res.kids);
      });
  }

  getComments = async kids => {
    let data = [];
    for await (const [idx, kid] of kids.entries()) {
      await fetch(`${process.env.REACT_APP_HN_API}item/${kid}.json`)
        .then(res => res.json())
        .then(async res => {
          if (res.deleted || res.dead) return;
          data[idx] = res;
          if (_.has(res, 'kids')) {
            data[idx].children = await this.getComments(res.kids);
          }
        })
    }

    return data;
  }

  render() {
    const { classes } = this.props;
    const { story, comments } = this.state;

    return (
      <React.Fragment>
        <StoryItem className={classes.story} story={story} />

        {/* {comments.map(comment => (
          <CommentItem key={comment.id} comment={comment} />
        ))} */}
      </React.Fragment>
    );
  }
}

export default injectSheet(styles)(Story);
