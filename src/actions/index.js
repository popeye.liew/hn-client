import { RSAA } from 'redux-api-middleware';

export const actionTypes = {
  GET_TOP_STORY_IDS_REQUEST: 'GET_TOP_STORY_IDS_REQUEST',
  GET_TOP_STORY_IDS_SUCCESS: 'GET_TOP_STORY_IDS_SUCCESS',
  GET_TOP_STORY_IDS_FAILURE: 'GET_TOP_STORY_IDS_FAILURE',

  GET_STORY_DETAILS_REQUEST: 'GET_STORY_DETAILS_REQUEST',
  GET_STORY_DETAILS_SUCCESS: 'GET_STORY_DETAILS_SUCCESS',
  GET_STORY_DETAILS_FAILURE: 'GET_STORY_DETAILS_FAILURE',

  SET_TOP_STORIES: 'SET_TOP_STORIES',

  CHANGE_PAGE: 'CHANGE_PAGE',
};

export const getTopStories = () => async dispatch => {
  // Get all the top story IDs
  await dispatch(getTopStoryIds());

  // Get each story's details
  return dispatch(getStoryDetails());
};

export const getTopStoryIds = () => dispatch => {
  return dispatch({
    [RSAA]: {
      endpoint: `${process.env.REACT_APP_HN_API}topstories.json`,
      method: 'GET',
      types: [
        actionTypes.GET_TOP_STORY_IDS_REQUEST,
        actionTypes.GET_TOP_STORY_IDS_SUCCESS,
        actionTypes.GET_TOP_STORY_IDS_FAILURE,
      ],
    }
  });
};

export const getStoryDetails = () => (dispatch, getState) => {
  const { topStoryIds, itemsPerPageLimit, currentPage } = getState().topStories;
  const startIdx = currentPage * itemsPerPageLimit;
  const endIdx = startIdx + itemsPerPageLimit;
  const storyIds = topStoryIds.slice(startIdx, endIdx);

  Promise.all(
    storyIds.map(storyId =>
      dispatch({
        [RSAA]: {
          endpoint: `${process.env.REACT_APP_HN_API}item/${storyId}.json`,
          method: 'GET',
          types: [
            actionTypes.GET_STORY_DETAILS_REQUEST,
            actionTypes.GET_STORY_DETAILS_SUCCESS,
            actionTypes.GET_STORY_DETAILS_FAILURE,
          ],
        },
      })
    )
  ).then(function(response) {
    return dispatch(setTopStories(response.map(resp => resp.payload)));
  });
};

export const setTopStories = payload => dispatch => {
  return dispatch({
    type: actionTypes.SET_TOP_STORIES,
    payload,
  });
};

export const changePage = payload => async dispatch => {
  // Update current page index
  dispatch({
    type: actionTypes.CHANGE_PAGE,
    payload,
  });

  // Get each story's details
  return dispatch(getStoryDetails());
}