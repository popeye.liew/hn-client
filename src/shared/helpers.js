import * as dateFns from 'date-fns';

const helpers = {
  getDomain(url) {
    try {
      return (new URL(url)).hostname;
    } catch (e) {
      return '';
    }
  },
  getSubmittedTime(timestamp) {
    const date = new Date(timestamp);
    const validDate = dateFns.isValid(date);
    return validDate ? `${dateFns.distanceInWordsToNow(date * 1000)} ago` : '';
  },
};
export default helpers;

export function createReducer(initialState, reducerMap) {
  return (state = initialState, action) => {
    const reducer = reducerMap[action.type];

    return reducer
            ? reducer(state, action.payload)
            : state;
  };
}