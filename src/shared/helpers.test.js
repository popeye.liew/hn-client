import helpers from 'shared/helpers';

describe('getDomain() should return the value as intended', () => {
  it('Valid URL format should return "www.hello.com"', () => {
    expect(helpers.getDomain('http://www.hello.com')).toBe('www.hello.com');
    expect(helpers.getDomain('http://hello.com')).toBe('hello.com');
  });
  it('Invalid URL format should return ""', () => {
    expect(helpers.getDomain('hello.com')).toBe('');
    expect(helpers.getDomain('123456')).toBe('');
    expect(helpers.getDomain('hello.com')).toBe('');
  });
});

describe('getSubmittedTime() should return the value as intended', () => {
  it('Invalid timestamp should return ""', () => {
    expect(helpers.getSubmittedTime('')).toBe('');
    expect(helpers.getSubmittedTime('asd@#$F')).toBe('');
  });
});