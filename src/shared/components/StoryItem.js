import React, { Component } from 'react';
import helpers from 'shared/helpers';
import { withRouter } from "react-router-dom";
import injectSheet from 'react-jss';

const styles = {
  root: {
    padding: '0.8rem 1.8rem',
    color: 'black',
    textDecoration: 'none',
    borderRadius: '5px',
    display: 'flex',
    alignItems: 'center',
    transition: '0.15s',
    cursor: 'pointer',
    '&:nth-of-type(even)': {
      backgroundColor: '#eee',
    },
    '&:hover': {
      backgroundColor: 'white',
      boxShadow: '0 0 15px 0 rgba(0, 0, 0, 0.1)',
      position: 'relative',
      zIndex: 2,
    },
  },
  upvoteContainer: {
    minWidth: '3rem',
    textAlign: 'center',
  },
  upvote: {
    borderStyle: 'solid',
    borderWidth: '0 10px 15px 10px',
    borderColor: 'transparent transparent #cccccc transparent',
    display: 'inline-block',
  },
  score: {
    marginTop: '0.2rem',
    fontSize: '1.2rem',
    fontWeight: 'bold',
  },
  content: {
    marginLeft: '1.8rem',
  },
  titleContainer: {
    margin: '0 0 0.2rem',
    fontSize: '1rem',
    fontWeight: 'bold',
  },
  title :{
    marginRight: '0.5rem',
  },
  source: {
    fontSize: '0.7rem',
    fontWeight: 400,
    color: '#ff3200',
    textDecoration: 'none',
    '&:hover': {
      textDecoration: 'underline',
    },
  },
  details: {
    fontSize: '0.7rem',
  },
  text: {
    margin: '1rem 0',
  },
  statistics: {
    fontSize: '0.7rem',
    marginTop: '0.5rem',
    '&:before': {
      content: '"💬"',
    },
  },
};

class StoryItem extends Component {
  goToStory = storyId => event => {
    if (event.target.localName !== 'a') this.props.history.push(`/stories/${storyId}`);
  }

  render() {
    const { classes, className, story } = this.props;

    return (
      <div className={[classes.root, className].join(' ')} onClick={this.goToStory(story.id)}>
        <div className={classes.upvoteContainer}>
          <span className={classes.upvote} />
          <div className={classes.score}>{story.score}</div>
        </div>

        <div className={classes.content}>
          <h2 className={classes.titleContainer}>
            <span className={classes.title}>{story.title}</span>
            {story.url && <a className={classes.source} href={story.url} target="_blank" rel="noopener noreferrer">({helpers.getDomain(story.url)})</a>}
          </h2>
          <div className={classes.details}>submitted {helpers.getSubmittedTime(story.time)}by {story.by}</div>
          {story.text && <div className={classes.text} dangerouslySetInnerHTML={{ __html: story.text }} />}
          <div className={classes.statistics}>
            <span className={classes.comments}>{story.descendants} comments</span>
          </div>
        </div>
      </div> 
    );
  }
}

export default withRouter(injectSheet(styles)(StoryItem));
