import React from 'react';
import helpers from 'shared/helpers';
import injectSheet from 'react-jss';

const styles = {
  root: {
    marginTop: '1.2rem',
    padding: '0.8rem 1.8rem',
    color: 'black',
    textDecoration: 'none',
    borderRadius: '5px',
    display: 'flex',
    '&:nth-child(n+3)': {
      marginTop: 0,
      paddingTop: '2rem',
      borderTop: '1px solid #ddd',
    },
  },
  upvoteContainer: {
    minWidth: '3rem',
    textAlign: 'center',
  },
  upvote: {
    borderStyle: 'solid',
    borderWidth: '0 10px 15px 10px',
    borderColor: 'transparent transparent #cccccc transparent',
    display: 'inline-block',
  },
  content: {
    marginLeft: '1.8rem',
  },
  details: {
    fontSize: '0.7rem',
  },
  text: {
    margin: '1rem 0',
  },
};

const CommentItem = ({ classes, className, comment }) => {
  return (
    <div className={[classes.root, className].join(' ')}>
      <div className={classes.upvoteContainer}>
        <span className={classes.upvote} />
      </div>

      <div className={classes.content}>
        <div className={classes.details}>submitted {helpers.getSubmittedTime(comment.time) }by {comment.by}</div>
        {comment.text && <div className={classes.text} dangerouslySetInnerHTML={{ __html: comment.text }} />}
      </div>
    </div> 
  );
}

export default injectSheet(styles)(CommentItem);
