import React from 'react';
import injectSheet from 'react-jss';

const styles = {
  root: {
    margin: '-0.2rem -0.2rem 1rem',
  },
  pageNumberItem: {
    width: '2rem',
    height: '2rem',
    margin: '0.2rem',
    lineHeight: '2rem',
    textAlign: 'center',
    color: '#777',
    backgroundColor: '#ccc',
    borderRadius: '50%',
    display: 'inline-block',
    transition: '0.15s',
    cursor: 'pointer',
    '&:hover': {
      backgroundColor: '#ff3200',
      color: 'white',
    },
  },
  pageNumberItemActive: {
    backgroundColor: '#ff3200',
    color: 'white',
  },
};

const Pagination = ({ classes, className, pagination, onClick }) => {
  let pageNumberItems = [];
  for (let i=0; i<(pagination.totalPageItems / pagination.itemsPerPageLimit); i++) {
    pageNumberItems.push(
      <span
        key={i}
        className={[classes.pageNumberItem, pagination.currentPage === i ? classes.pageNumberItemActive : ''].join(' ')}
        onClick={() => onClick(i)}
      >
        {i + 1}
      </span>
    );
  }

  return (
    <div className={[classes.root, className].join(' ')}>{pageNumberItems}</div> 
  );
}

export default injectSheet(styles)(Pagination);
