# HN Client [![Version](https://img.shields.io/badge/version-0.1.2-brightgreen.svg)](https://img.shields.io/badge/version-0.1.2-brightgreen.svg)
This is a web application that lists top 500 stories from [HackerNews](https://news.ycombinator.com).

## Table of Contents
- [Features](#features)
- [What's Next](#whats-next)
- [Stack Used](#stack-used)
- [Installation](#installation)
- [Development](#development)
- [Unit Tests](#unit-tests)
- [Code Coverage](#code-coverage)

## Features
- List stories from HN
- Pagination on top stories listing
- Half-baked single story page

## What's Next
- Comment section in single story page
- Unit & automated tests
- CI/CD

## Stack Used
- React bootstrapped with [Create React App](https://github.com/facebook/create-react-app)
- [React Router](https://github.com/ReactTraining/react-router)
- [JSS](https://github.com/cssinjs/react-jss)
- [HackerNews API](https://github.com/HackerNews/API)

## Installation
Clone this project and install the dependencies with `npm`:
```
npm ci
```

## Development
Run the command below to start a development server at `http://localhost:3000.
Have fun!
```
npm start
```

## Unit Tests
To run unit tests:
```
npm run test
```

## Code Coverage
To check code coverage, simply run:
```
npm run coverage
```